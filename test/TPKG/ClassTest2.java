/*
 * MUHAMMAD Ahsan
 * <muhammad.ahsan@gmail.com>
 */
package TPKG;

import SPKG.NewClass;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author ahsan
 */
@RunWith(Parameterized.class)
public class ClassTest2 {

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
    private int param = 0;

    public ClassTest2(int testParameter) {
        this.param = testParameter;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{4}, {5}, {6}};
        return Arrays.asList(data);
    }

    @Test
    public void test2F() {
        System.out.println("test2 with param = " + param);
        NewClass instance = new NewClass();
        int expResult = 2;
        int result = instance.f2(param);
        assertEquals(expResult, result);
    }
}
