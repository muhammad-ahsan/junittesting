/*
 * MUHAMMAD Ahsan
 * <muhammad.ahsan@gmail.com>
 */
package TPKG;

import SPKG.NewClass;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * @see http://www.vogella.com/tutorials/JUnit/article.html
 * @author ahsan
 */
@RunWith(Parameterized.class)
public class ClassTest1 {

    /* Each test executed with one test class instance */
    @BeforeClass
    public static void setUpClass() {
//        System.out.println("setUpClass() : 1 time in JUnit test class");
    }

    @AfterClass
    public static void tearDownClass() {
//        System.out.println("tearDownClass(): 1 time in JUnit test class");
    }

    @Before
    public void setUp() {
//        System.out.println("setUp(): every time in JUnit test class");
    }

    @After
    public void tearDown() {
//        System.out.println("tearDown(): every time in JUnit test class");
    }

    private final int param;

    public ClassTest1(int testParameter) {
        this.param = testParameter;
    }
    /*
     JUnit - Parameterized Test
     --------------------------
     1: Annotate test class with @RunWith(Parameterized.class)
     2: Create a public static method annotated with @Parameters that returns a Collection of Objects (as Array) as test data set.
     3: Create a public constructor that takes in what is equivalent to one "row" of test data.
     4: Create an instance variable for each "column" of test data.
     5: Create your tests case(s) using the instance variables as the source of the test data.
     6: The test case will be invoked once per each row of data. Let's see Parameterized tests in action.
     */

    @Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{1}, {2}, {3}};
        return Arrays.asList(data);
    }

    /**
     * Test of f method, of class HeavyNumber. Test will execute for each
     * argument @Parameter
     */
    @Test
    public void test1F() {
        System.out.println("test1 with param = " + param);
        NewClass instance = new NewClass();
        int expResult = 1;
        int result = instance.f1(param);
        assertEquals(expResult, result);
    }
}
