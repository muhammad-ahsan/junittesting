/*
 * MUHAMMAD Ahsan
 * <muhammad.ahsan@gmail.com>
 */
package TPKG;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author ahsan
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({TPKG.ClassTest1.class, TPKG.ClassTest2.class})
public class NewTestSuite1 {

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

}
